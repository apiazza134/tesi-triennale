#!/usr/bin/env python

import os
import numpy as np

import utils

files = [os.path.join('build/out', f) for f in os.listdir('build/out')]

T, u, m = np.empty((3, len(files)))
for i, f in enumerate(files):
    step, _u, _m = np.loadtxt(f, unpack=True)
    T[i] = utils.get_parameters(f)['T']
    u[i] = np.mean(_u)
    m[i] = np.mean(np.abs(_m))

# Plots
title = utils.title_from_config(files[0], 'L')
plt_kwargs = dict(color='black', linestyle='', marker='.', markersize=1)

utils.plot(
    T, u,
    xlabel='$T$', ylabel=r'$\langle u \rangle$', title=title,
    save_fname='build/energy-T.png',
    show=True,
    **plt_kwargs
)

utils.plot(
    T, m,
    xlabel='$T$', ylabel=r'$\langle |m| \rangle$', title=title,
    save_fname='build/magnetization-T.png',
    show=True,
    **plt_kwargs
)
