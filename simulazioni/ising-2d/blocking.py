#!/usr/bin/env python

import sys
import os
import numpy as np
import utils

BLOCK_SIZE = 200

outdir = sys.argv[1]
files = [os.path.join(outdir, f) for f in os.listdir(outdir)]

L = utils.get_parameters(files[0])['L']

T, u, du, m, dm = np.empty((5, len(files)))
for i, f in enumerate(files):
    step, _u, _m = np.loadtxt(f, unpack=True)

    _u = utils.blocking(_u, BLOCK_SIZE)
    _m = utils.blocking(np.abs(_m), BLOCK_SIZE)

    T[i] = utils.get_parameters(f)['T']
    u[i] = np.mean(_u)
    du[i] = utils.mean_std(_u)
    m[i] = np.mean(_m)
    dm[i] = utils.mean_std(_m)

utils.write_arrays(
    (T, u, du, m, dm),
    f'../fss/blocking/{L}',
    header='T  <u>  sigma(<u>)  <|m|>  sigma(<m>)'
)

# Plots
plt_kwargs = dict(color='blue', linestyle='', marker='.', markersize=4)
title = f'$L = {L}$'

utils.plot(
    T, u, yerr=du,
    xlabel='$T$', ylabel=r'$\langle u \rangle$', title=title,
    save_fname='build/blocking-energy.png',
    show=True,
    **plt_kwargs
)

utils.plot(
    T, m, yerr=dm,
    xlabel='$T$', ylabel=r'$\langle |m| \rangle$', title=title,
    save_fname='build/blocking-magnetization.png',
    show=True,
    **plt_kwargs
)
