#!/usr/bin/env python

import sys
import numpy as np
import utils

step, u, m = np.loadtxt(sys.argv[1], unpack=True)

absm = np.abs(m)
N_SAMPLE = len(step)

title = utils.title_from_config(sys.argv[1], 'L', 'T')

# Std error compute with bocking varing the block size
blocks = range(1, 1000)

err = np.empty(len(blocks))
mean = np.empty(len(blocks))
for i, block in enumerate(blocks):
    blocked_absm = utils.blocking(absm, block)
    err[i] = utils.mean_std(blocked_absm)

utils.plot(
    blocks, err,
    xlabel='block size', ylabel=r'$\sigma(\langle |m|\rangle)$', title=title,
    save_fname='build/blocking-size.png',
    show=True,
    color='black'
)

# Std error compute with bocking varing the blocking trasformation
# iteration (blocking size fixed)
block = 2
d = int(np.log(N_SAMPLE) / np.log(block))

M = np.empty(d)
dM = np.empty(d)

for i in range(d):
    M[i] = np.mean(absm)
    dM[i] = utils.mean_std(absm)

    step = utils.blocking(step, block)
    absm = utils.blocking(absm, block)

utils.plot(
    range(d), dM,
    xlabel='iterations', ylabel=r'$\sigma(\langle |m|\rangle)$', title=title,
    save_fname='build/blocking-iter.png',
    show=True,
    color='black'
)
