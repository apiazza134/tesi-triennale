#include <stdlib.h>
#include <stdio.h>
#include <sys/stat.h>
#include <sys/types.h>

#include "ising-2d.h"

#define RUNS 10

int main(void)
{
    double T = 2.1;
    double B = 0.;

    mkdir("eq", 0766);

    for (int i = 0; i < RUNS; i++) {
        spin ** S = init_state_high_temp();

        char buffer[32];
        snprintf(buffer, 32, "eq/out-%d.txt", i);

        MonteCarlo(1 / T, B, S, buffer);

        free(S);
    }

    return 0;
}
