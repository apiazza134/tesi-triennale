#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <math.h>

#include "ising-2d.h"

#define STEP 20
#define nu 1.
#define Tc (2. * J / log(1 + sqrt(2)))

double fss_T(double xi)
{
    return (Tc * xi / pow(L, 1./nu)) + Tc;
}

int main(void)
{
    double T0 = fss_T(-2);
    double T1 = fss_T(4);
    double B = 0.;

    mkdir("out", 0766);

    for (int i = 0; i < STEP; i++) {
        double T = (double) T0 + i * (T1 - T0) / STEP;
        char buffer[32];
        snprintf(buffer, 32, "out/%lf", T);

        printf("T_%d = %lf\n", i, T);

        spin ** S = init_state_high_temp();
        MonteCarlo(1 / T, B, S, buffer);
        free_state(S);
    }

    return 0;
}
