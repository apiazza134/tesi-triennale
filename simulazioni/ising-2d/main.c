#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#include "ising-2d.h"

int main(void)
{
    double T = 2. * J / log(1 + sqrt(2));
    double B = 0.;

    spin ** S = init_state_high_temp();

    MonteCarlo(1 / T, B, S, "out.txt");

    free_state(S);

    return 0;
}
