#!/usr/bin/env python

import os
import numpy as np
from PIL import Image

os.makedirs('build/imgs', exist_ok=True)

N_FRAMES = len(os.listdir('build/frames'))

fnumbers = sorted(list(
    map(lambda s: int(s), os.listdir('build/frames'))
))

i = 0
for frame in fnumbers:
    state = np.loadtxt(f'build/frames/{frame}', unpack=True)
    state = ((1 + state)/2).astype(np.uint8)

    img = Image.new('1', state.shape)
    img.putdata(state.flatten())
    img.thumbnail((4 * state.shape[0], 4 * state.shape[1]))
    img.save(f'build/imgs/{i}.png')
    img.close()

    i += 1
