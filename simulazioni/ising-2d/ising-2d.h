#ifndef ISING_2D_H
#define ISING_2D_H

#define L 35
#define N (L*L)
#define z 4
#define J 1.

#define N_STEP 1006000

#define FRAME_FACTOR 1.01

#define SAMPLE_START 6000
#define SAMPLE_STEP 10
#define SAMPLE_SIZE ((N_STEP - SAMPLE_START) / SAMPLE_STEP)

typedef enum {up = 1, down = -1} spin;

extern const char FRAMES_DIR[7];

long int write_frame_to_file(spin ** state, long int frame);
void print_state(spin ** state);

char * parameters(double T, double B);

void free_state(spin ** state);

spin ** init_state_low_temp(spin state);
spin ** init_state_high_temp(void);
spin ** init_state_interface(void);

double energy(spin ** state, double B);
int magnetization(spin ** state);

void correlation_function(double chi[SAMPLE_SIZE], int M[SAMPLE_SIZE]);

spin ** MonteCarlo(double beta, double B, spin ** state, char * out_observables);

#endif // ISING_2D_H
