#!/usr/bin/env python

import sys
import utils
import numpy as np

BLOCK_SIZE = 200

step, u, m = np.loadtxt(sys.argv[1], unpack=True)
m = np.abs(m)
N_SAMPLE = len(step)
N_BLOCKS = int(N_SAMPLE / BLOCK_SIZE)
L = utils.get_parameters(sys.argv[1])['L']

dc = np.empty(2000-2)
dchi = np.empty(2000-2)

for N_RESAMPLING in range(2, 2000):
    _u = np.reshape(u, (N_BLOCKS, BLOCK_SIZE))
    _m = np.reshape(m, (N_BLOCKS, BLOCK_SIZE))

    cB = np.empty(N_RESAMPLING)
    chiB = np.empty(N_RESAMPLING)
    for j in range(N_RESAMPLING):
        rand_idx = np.random.randint(0, high=N_BLOCKS, size=N_BLOCKS)
        uB = u[rand_idx].flatten()
        mB = m[rand_idx].flatten()
        cB[j] = (L * utils.std(uB))**2
        chiB[j] = (L * utils.std(mB))**2

    dc[N_RESAMPLING-2] = utils.std(cB)
    dchi[N_RESAMPLING-2] = utils.std(chiB)

# utils.write_arrays(
#     (range(2, 2000), dc, dchi),
#     'build/python-out/bootstrap-var.txt',
#     header='N_RESAMPLING  sigma_c^2  sigma_chi^2'
# )

title = utils.title_from_config(sys.argv[1], 'L', 'T')

utils.plot(
    range(2, 2000), dc,
    xlabel='resampling size', ylabel=r'$\sigma^2_c$', title=title,
    save_fname='build/bootstrap-resampling-size-c.png',
    show=True,
    color='black'
)

utils.plot(
    range(2, 2000), dchi,
    xlabel='resampling size', ylabel=r'$\sigma^2_\chi$', title=title,
    save_fname='build/bootstrap-resampling-size-chi.png',
    show=True,
    color='black'
)
