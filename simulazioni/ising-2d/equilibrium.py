#!/usr/bin/env python

import os
import numpy as np
import matplotlib.pyplot as plt

plt.style.use('../style.yml')

for f in os.listdir('build/eq'):
    step, u, m = np.loadtxt(os.path.join('build/eq', f), unpack=True)

    plt.figure("energy")
    plt.plot(step, u, color='black')

    plt.figure("magnetization")
    plt.plot(step, abs(m), color='black')


plt.figure("magnetization")
plt.xlabel("step")
plt.ylabel("$|m|$")
plt.savefig('build/eq.png')
