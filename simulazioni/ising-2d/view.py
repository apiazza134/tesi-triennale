#!/usr/bin/env python

import sys
import numpy as np
import matplotlib.pyplot as plt

import utils

plt.rcParams['agg.path.chunksize'] = 10000

if len(sys.argv) == 2:
    data_fname = sys.argv[1]
else:
    data_fname = 'build/out.txt'

step, u, m = np.loadtxt(data_fname, unpack=True)

title = utils.title_from_config(data_fname, 'L', 'T')
utils.plot(
    step, u,
    xlabel='$t$', ylabel='$u$', title=title,
    save_fname='build/energy.png',
    show=True,
    color='black'
)

utils.plot(
    step, m,
    xlabel='$t$', ylabel='$m$', title=title,
    # save_fname='build/magnetization.png',
    show=True,
    color='black'
)

# Time correlation function
# step, chi = np.loadtxt('build/correlations.txt', unpack=True)

# step = step[:int(len(chi)/20)]
# chi = chi[:int(len(chi)/20)]

# if chi[0] != 0:
#     chi = chi / chi[0]
# utils.plot(
#     step, chi,
#     xlabel='$t$', ylabel=r'$\chi/\chi(0)$', title=title, save_fname='build/correlation.png',
#     show=True,
#     color='black'
# )
