#!/usr/bin/env python

import sys
import os
import numpy as np
import utils

BLOCK_SIZE = 200
N_RESAMPLING = 1000

outdir = sys.argv[1]
files = [os.path.join(outdir, f) for f in os.listdir(outdir)]
L = utils.get_parameters(files[0])['L']

T, c, dc, chi, dchi = np.empty((5, len(files)))

for i, f in enumerate(files):
    print(f)
    step, u, m = np.loadtxt(f, unpack=True)
    m = np.abs(m)
    N_SAMPLE = len(u)
    N_BLOCKS = int(N_SAMPLE / BLOCK_SIZE)

    cB = np.empty(N_RESAMPLING)
    chiB = np.empty(N_RESAMPLING)

    u = np.reshape(u, (N_BLOCKS, BLOCK_SIZE))
    m = np.reshape(m, (N_BLOCKS, BLOCK_SIZE))

    for j in range(N_RESAMPLING):
        rand_idx = np.random.randint(0, high=N_BLOCKS, size=N_BLOCKS)
        uB = u[rand_idx].flatten()
        mB = m[rand_idx].flatten()
        cB[j] = (L * utils.std(uB))**2
        chiB[j] = (L * utils.std(mB))**2

    T[i] = utils.get_parameters(f)['T']
    c[i] = np.mean(cB)
    dc[i] = utils.std(cB)
    chi[i] = np.mean(chiB)
    dchi[i] = utils.std(chiB)


utils.write_arrays(
    (T, c, dc, chi, dchi),
    f'../fss/bootstrap/{L}',
    header='T  <c>  sigma(<c>)  <chi>  sigma(<chi>)'
)

# Plots
plt_kwargs = dict(color='blue', linestyle='', marker='.', markersize=4)
title = utils.title_from_config(
    files[0], 'L', before_params=f'$b = {BLOCK_SIZE}$'
)

utils.plot(
    T, c, yerr=dc,
    xlabel='$T$', ylabel='$c$', title=title,
    save_fname='build/bootstrap-specific-heat.png',
    show=True,
    **plt_kwargs
)

utils.plot(
    T, chi, yerr=dchi,
    xlabel='$T$', ylabel=r'$\chi$', title=title,
    save_fname='build/bootstrap-susceptibility.png',
    show=True,
    **plt_kwargs
)
