#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <stdbool.h>
#include <sys/types.h>
#include <sys/stat.h>

#include <pcg_variants.h>

#include "ising-2d.h"

const char FRAMES_DIR[7] = "frames";

double pcg64_random_double(pcg64_random_t * rng_ptr)
{
    return ldexp(pcg64_random_r(rng_ptr), -64);
}

long int write_frame_to_file(spin ** state, long int frame)
{
    char buffer[32];
    snprintf(buffer, 32, "%s/%ld", FRAMES_DIR, frame);
    FILE *fp = fopen(buffer, "w");

    for (int i = 0; i < L; i++) {
        for (int j = 0; j < L; j++) {
            fprintf(fp, "%d ", state[i][j]);
        }
        fprintf(fp, "\n");
    }

    fclose(fp);

    long int new_frame = frame * FRAME_FACTOR;
    if (new_frame == frame) {
        return frame + 1;
    } else {
        return new_frame;
    }
}

char * parameters(double beta, double B)
{
    char * buffer = malloc(128 * sizeof(char));
    snprintf(
        buffer, 128,
        "L: %d, T: %lf, beta: %lf, B: %lf, J: %lf",
        L, 1./beta, beta, B, J
        );
    return buffer;
}

void print_state(spin ** state)
{
    for (int i = 0; i < L; i++) {
        for (int j = 0; j < L; j++) {
            printf(state[i][j] == up ? " 1 " : "-1 ");
        }
        printf("\n");
    }
}

void free_state(spin ** state)
{
    for (int i = 0; i < L; i++)
        free(state[i]);
    free(state);
}

int Lmod(int n)
{
    return ((n % L) + L) % L;
}

spin ** init_state_low_temp(spin state)
{
    spin ** S = malloc(L * sizeof(spin *));
    for (int i = 0; i < L; i++) {
        S[i] = malloc(L * sizeof(spin));
        for (int j = 0; j < L; j++) {
            S[i][j] = state;
        }
    }

    return S;
}

spin ** init_state_high_temp(void)
{
    spin ** S = malloc(L * sizeof(spin *));

    pcg64_random_t rng;
    pcg64_srandom_r(&rng, time(NULL), (intptr_t)&rng);

    for (int i = 0; i < L; i++) {
        S[i] = (spin*) malloc(L * sizeof(spin));
        for (int j = 0; j < L; j++) {
            S[i][j] = pcg64_boundedrand_r(&rng, 2) ? up : down;
        }
    }

    return S;
}

spin ** init_state_interface(void)
{
    spin ** S = malloc(L * sizeof(spin *));
    for (int i = 0; i < L; i++) {
        S[i] = malloc(L * sizeof(spin));
        for (int j = 0; j < L / 2; j++) {
            S[i][j] = up;
        }
        for (int j = L / 2; j < L; j++) {
            S[i][j] = down;
        }
    }

    return S;
}

double energy(spin ** state, double B)
{
    double sum = 0;

    for (int i = 0; i < L; i++) {
        for (int j = 0; j < L; j++) {
            sum += state[i][j] * (J * (state[i][(j + 1) % L] + state[(i + 1) % L][j]) + B);
        }
    }

    return - sum;
}

int magnetization(spin ** state)
{
    int sum = 0;
    for (int i = 0; i < L; i++) {
        for (int j = 0; j < L; j++) {
            sum += state[i][j];
        }
    }

    return sum;
}


void correlation_function(double chi[SAMPLE_SIZE], int M[SAMPLE_SIZE])
{
    long int sum_M = 0, sum_M_shifted = 0;
    for (int i = SAMPLE_SIZE - 1; i >= 0; i--) {
        int cutoff = SAMPLE_SIZE - i;

        long int convolution_M = 0;
        for (int k = 0; k < cutoff; k++)
            convolution_M += abs(M[k] * M[k + i]);

        sum_M += abs(M[cutoff - 1]);
        sum_M_shifted += abs(M[i]);

        chi[i] = (double) convolution_M/N/N/cutoff - ((double) sum_M/N/cutoff) * ((double) sum_M_shifted/N/cutoff);
    }
}


spin ** MonteCarlo(double beta, double B, spin ** state, char * out_observables)
{
    /* Acceptance ratio and energies should be a (1 dimensional/non nested) dictionary but it's too painful to implement in C. Therefore we convert the sum over the next neighbours to an index in the array and vice versa: the rule is sum_nn[i] = z - 2*i. */
    double partial_deltaE[z+1]; // without s_k
    for (int i = 0; i < z+1; i++) {
        partial_deltaE[i] = (double) 2*(J * (z - 2*i) + B);
    }

    double A[z+1];
    for (int i = 0; i < z+1; i++) {
        A[i] = exp(- beta * fabs(partial_deltaE[i]));
    }

    // Energy and magnetization
    double U[SAMPLE_SIZE] = {};
    int M[SAMPLE_SIZE] = {};

    // Creaiting frames directory and declating frame number
    mkdir(FRAMES_DIR, 0766);
    long int frame = 0;

    bool accepted = false;

    pcg64_random_t rng;
    pcg64_srandom_r(&rng, time(NULL), (intptr_t)&rng);

    for (long int step = 0; step < N_STEP; step++) {
        // Write things to file
        if (step == frame) {
            frame = write_frame_to_file(state, frame);
        }

        // Compute energy and magnetization
        if (step >= SAMPLE_START && (step - SAMPLE_START) % SAMPLE_STEP == 0) {
            int idx = (step - SAMPLE_START) / SAMPLE_STEP;
            U[idx] = energy(state, B);
            M[idx] = magnetization(state);
        }

        // Monte Carlo step
        for (int site_step = 0; site_step < N; site_step++) {
            // Flip a spin randomly
            int i = pcg64_boundedrand_r(&rng, L);
            int j = pcg64_boundedrand_r(&rng, L);
            spin spin_tbf = state[i][j]; // spin to be flipped

            // Compute the sum over next neighbours (with periodic boundary)
            int sum_nn = state[Lmod(i + 1)][j] + state[i][Lmod(j + 1)] \
                + state[Lmod(i - 1)][j] + state[i][Lmod(j - 1) % L];

            int index = (z - sum_nn)/2;
            double deltaE = spin_tbf * partial_deltaE[index];

            // Accept or reject the new configuration
            if (deltaE <= 0) {
                accepted = true;
            } else if (pcg64_random_double(&rng) < A[index]) {
                accepted = true;
            }

            // Update configuration and observables
            if (accepted) {
                state[i][j] = (spin_tbf == up) ? down : up;
                accepted = false;
            }
        }
    }

    // Writing last frame
    write_frame_to_file(state, N_STEP - 1);

    // Time correlations
    /* double chi[SAMPLE_SIZE] = {}; */
    /* correlation_function(chi, M); */

    // Write observables to file
    FILE *fp = fopen(out_observables, "w");
    /* FILE *fpc = fopen("correlations.txt", "w"); */
    fprintf(fp, "# %s\n", parameters(beta, B));
    for (int i = 0; i < SAMPLE_SIZE; i++) {
        fprintf(fp, "%ld %lf %lf\n", (long int) SAMPLE_START + i*SAMPLE_STEP, (double) U[i] / N, (double) M[i] / N);
        /* fprintf(fpc, "%ld %lf\n", (long int) SAMPLE_START + i*SAMPLE_STEP, chi[i]); */
    }
    fclose(fp);
    /* fclose(fpc); */

    return state;
}
