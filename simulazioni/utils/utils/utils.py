import yaml
import os
import numpy as np
import matplotlib.pyplot as plt


def yaml_load(filename: str) -> dict:
    """Wrapper aroud yaml.safe_load
    """
    with open(filename, 'r') as f:
        return yaml.safe_load(f)


def get_parameters(filename: str) -> dict:
    """Get parameters from first line of a data file. Parameters are
    expected to be present in the first line in form
    # param1: value1, param2: value2, ...
    """

    with open(filename, "r") as f:
        first_line = f.readline()

    if first_line[0] == '#':
        return yaml.safe_load(first_line.strip('# ').replace(', ', '\n'))

    return None


def blocking(x: np.array, block: int) -> np.array:
    """Applay blocking transformation to x with block size block
    """
    size = int(len(x) / block)
    y = np.empty(size)

    for i in range(size):
        y[i] = np.mean(x[(i * block):(i * block + block)])

    return y


def proper_divisors(n: int) -> [int]:
    """Compute proper divisors of n
    """
    divisors = []
    for i in range(1, int(n/2) + 1):
        if n % i == 0:
            divisors.append(i)

    return divisors


def std(x: np.array) -> float:
    """Compute the unbiased standard deviation of x. The values in x are
    assumed to be uncorrelated
    """
    return np.std(x, ddof=1)


def mean_std(x: np.array) -> float:
    """Compute the standard deviation of the mean of x. The values in x are
    assumed to be uncorrelated
    """
    return std(x) / np.sqrt(len(x))


def style_use():
    """Sets plot style in all subsequents plots
    """
    style = {
        'text.usetex': True,
        'text.latex.preamble': r'\usepackage[euler-digits]{eulervm}',
        'font.family': 'serif',
        'font.size': 15,
        'axes.titlesize': 17,
        'axes.labelsize': 17,
        'lines.linewidth': 0.5,
        'savefig.dpi': 600,
        'savefig.bbox': 'tight'
    }
    plt.style.use(style)


def plot(x, y, xerr=None, yerr=None, xlabel: str = None, ylabel: str = None,
         title: str = None, save_fname: str = None, show: bool = False,
         **plt_kwargs):
    """Wrapper around plt.errorbar (if either one of the two
    errors is not None). Also set figure style, but this will affect
    all subsequents plots as a side effect: to restore the normal plot
    style use plt.style.use('classic') after calling this function.
    """
    style_use()

    plt.errorbar(x, y, xerr=xerr, yerr=yerr, **plt_kwargs)

    if xlabel:
        plt.xlabel(xlabel)
    if ylabel:
        plt.ylabel(ylabel)
    if title:
        plt.title(title)
    if save_fname:
        plt.savefig(save_fname)
    if show:
        plt.show()

    plt.close()


def plot_with_yerrors(x, y, xerr=None, yerr=None, xlabel: str = None,
                      ylabel: str = None, title: str = None, save_fname: str =
                      None, show: bool = False, **plt_kwargs):
    """Like `utils.plot` but will also display (x, yerr) just below the
    (x, y) graph.

    """
    style_use()

    fig, ax = plt.subplots(2, 1, sharex=True,
                           gridspec_kw={'hspace': 0, 'height_ratios': [3, 1]})
    ax[0].errorbar(x, y, xerr=xerr, yerr=yerr, **plt_kwargs)
    ax[1].plot(x, yerr, linestyle='', marker='x', markersize=2, color='black')

    if xlabel:
        ax[1].set_xlabel(xlabel)
    if ylabel:
        ax[0].set_ylabel(ylabel)
    if title:
        ax[0].set_title(title)
    if save_fname:
        plt.savefig(save_fname)
    if show:
        plt.show()

    plt.close()


def title_from_config(data_fname: str, *args, before_params: str = None) -> str:
    config = get_parameters(data_fname)

    title = [before_params] if before_params else []
    for key in args:
        if key in config:
            title.append(
                f'${key} = {config[key]}$'
            )
        else:
            raise KeyError("{key} not found in {config_fname}.")

    return ', '.join(title)


def write_arrays(arrays: (np.array), fname: str, header: str = ''):
    """Wrapper around `np.savetxt`. Puts every element of arrays as a
    column.  Also creates eventual non existing directory in `fname`.
    """
    towrite = np.stack(arrays, axis=1)

    directory, filename = os.path.split(fname)
    os.makedirs(directory, exist_ok=True)

    np.savetxt(fname, towrite, header=header)
