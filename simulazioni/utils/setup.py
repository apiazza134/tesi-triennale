from setuptools import setup, find_packages
from os import path

here = path.abspath(path.dirname(__file__))

setup(
    name="utils",
    version="0.0.1",
    description="Python utilities for tesi-triennale",
    author="Alessandro Piazza",
    packages=find_packages(),
    python_requires='>=3.8.5',
    install_requires=[
        'pyyaml',
        'numpy',
        'matplotlib',
    ]
)
