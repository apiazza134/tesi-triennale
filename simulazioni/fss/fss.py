#!/usr/bin/env python

import os
import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
import utils

sizes = sorted(list(map(
    lambda x: int(x),
    os.listdir('blocking')
)))

utils.style_use()

for L in sizes:
    T, u, du, m, dm = np.loadtxt(os.path.join('blocking', str(L)), unpack=True)
    T, c, dc, chi, dchi = np.loadtxt(os.path.join('bootstrap', str(L)), unpack=True)

    plt_kwargs = dict(
        marker='.', markersize=4, linestyle='',
        label=f'${L}$'
    )

    plt.figure('u')
    plt.errorbar(T, u, yerr=du, **plt_kwargs)

    plt.figure('m')
    plt.errorbar(T, m, yerr=dm, **plt_kwargs)

    plt.figure('c')
    plt.errorbar(T, c, yerr=dc, **plt_kwargs)

    plt.figure('chi')
    plt.errorbar(T, chi, yerr=dchi, **plt_kwargs)


legend_kwargs = dict(title='$L$', loc='upper right', bbox_to_anchor=(1, 1))

plt.figure('u')
plt.legend(**legend_kwargs)
plt.xlabel('$T$')
plt.ylabel(r'$\langle u \rangle$')
plt.savefig('blocking-u-fss.png')

plt.figure('m')
plt.legend(**legend_kwargs)
plt.xlabel('$T$')
plt.ylabel(r'$\langle |m| \rangle$')
plt.savefig('blocking-m-fss.png')

plt.figure('c')
plt.legend(**legend_kwargs)
plt.xlabel('$T$')
plt.ylabel('$c$')
plt.savefig('bootstrap-c-fss.png')

plt.figure('chi')
plt.legend(**legend_kwargs)
plt.xlabel('$T$')
plt.ylabel(r'$\chi$')
plt.savefig('bootstrap-chi-fss.png')

plt.show()

Tc = 2/np.log(1 + np.sqrt(2))
nu = 1
gamma = 7/4

xi, chi0, dchi0 = np.empty((3, 0))
chi_max, dchi_max = np.empty((2, 0))

for L in sizes:
    T, c, dc, chi, dchi = np.loadtxt(os.path.join('bootstrap', str(L)), unpack=True)

    _xi = L**(1/nu) * (T - Tc) / Tc
    _chi0 = chi / L**(gamma/nu)
    _dchi0 = dchi / L**(gamma/nu)

    plt.errorbar(
        _xi, _chi0, yerr=_dchi0,
        marker='.', markersize=4, linestyle='', label=L
    )

    chi_max = np.append(chi_max, np.max(chi))
    dchi_max = np.append(dchi_max, np.max(dchi))

    xi = np.append(xi, _xi)
    chi0 = np.append(chi0, _chi0)
    dchi0 = np.append(dchi0, _dchi0)

plt.xlabel(r'$y$')
plt.ylabel(r'$\chi_0(y)$')
plt.savefig('fss.png')
plt.show()
plt.close()


def peak(x, gamma_nu, a):
    return a * x**(gamma_nu)


opt, cov = curve_fit(
    peak, sizes, chi_max, p0=(1, gamma/nu), sigma=dchi_max, absolute_sigma=True
)

gamma = opt[0]
dgamma = np.sqrt(np.diagonal(cov))[0]

# chi2 = np.sum(((chi_max - peak(sizes, *opt))/dchi_max)**2)
# ddof = len(sizes) - len(opt)
# print(f'{chi2} \\pm {np.sqrt(2 * ddof)} / {ddof}')

x = np.logspace(np.log(sizes[0]) - 0.1, np.log(sizes[-1]) + 0.1, base=np.e)
plt.plot(x, peak(x, *opt), color='blue')

plt.errorbar(
    sizes, chi_max, yerr=dchi_max,
    marker='.', markersize=4, linestyle='', color='black'
)
plt.xlabel(r'$L$')
plt.ylabel(r'$\chi(y_0, L)$')
plt.xscale('log')
plt.yscale('log')
plt.title(f'$\\gamma = {gamma:.2f} \\pm {dgamma:.2f}$')
plt.savefig('fss-gamma.png')
plt.show()
