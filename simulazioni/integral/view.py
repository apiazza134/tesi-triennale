#!/usr/bin/env python

import numpy as np
from utils import plot

x, Imc = np.loadtxt('build/out.txt', unpack=True)

plot(
    x, Imc,
    xlabel='$x$', ylabel='$I(x)$',
    title='Integral of $\\sin^{2}(1/x)$ via MC method',
    save_fname='build/integral-mc.png',
    color='blue'
)
