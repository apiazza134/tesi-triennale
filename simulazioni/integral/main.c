#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <limits.h>
#include <float.h>

#define n 1000
#define x0 -1.
#define xn 1.
#define yn 1.

#define N 1000000

double func(double x)
{
    if (fabs(x) < DBL_EPSILON) {
    }
    return pow(sin(1/x), 2);
}

int main(void)
{
    printf("Ensure that yn = %lf is higher than the maxmim of func in [x0, xn] = [%lf, %lf]\n\n", yn, x0, xn);

    srand(time(NULL));

    FILE* fptr = fopen("out.txt", "w");

    double Imc[n+1];

    Imc[0] = 0;

    double bin_size = (xn - x0)/n;

    for (int i = 0; i < n; i++) {
        double x = x0 + i * bin_size;

        int M = 0;
        for (int j = 0; j < N; j++) {
            double h = x + bin_size * ((double) rand()/RAND_MAX);
            double v = yn * ((double) rand()/RAND_MAX);

            if (v < func(h)) {
                M++;
            }
        }

        Imc[i+1] = yn * bin_size * ((double) M/N) + Imc[i];

        fprintf(fptr, "%lf %lf\n", x + bin_size, Imc[i]);
    }

    fclose(fptr);

    return 0;
}
