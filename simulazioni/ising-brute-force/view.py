#!/usr/bin/env python

import numpy as np
from utils import plot

T, c = np.loadtxt('build/out.txt', unpack=True)

plot(
    T, c,
    xlabel='$T$', ylabel='$c$',
    save_fname='build/specific-heat.png',
    color='blue', marker='.', markersize=1
)
