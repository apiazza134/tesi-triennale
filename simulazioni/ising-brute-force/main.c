#include <math.h>
#include <stdio.h>
#include <stdlib.h>

#define J 1.
#define n 5 // Don't push this over 5
#define N (n*n)

typedef enum {up = 1, down = -1} spin;

/* double hamiltonian(spin state[n][n]) */
/* { */
/*     int sum = 0; */

/*     for (int i = 0; i < n; i++) { */
/*         for (int j = 0; j < n; j++) { */
/*             sum += state[i][j] * (state[i][(j + 1) % n] + state[(i + 1) % n][j]); */
/*         } */
/*     } */

/*     return - J * ((double) sum); */
/* } */

void print_state(spin state[n][n])
{
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < n; j++) {
            printf("%d ", state[i][j]);
        }
        printf("\n");
    }
    printf("\n");
}

void print_inverted_state(spin state[n][n])
{
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < n; j++) {
            printf("%d ", -state[i][j]);
        }
        printf("\n");
    }
    printf("\n");
}

double specific_heat(double beta, double * H, int half_states_size)
{
    double Z = 0;

    spin S[n][n];
    for (int s = 0; s < half_states_size; s++) {
        for (int k = 0, mask = pow(2, N-1); k < N && mask > 0; k++, mask >>= 1) {
            int i = k / n, j = k % n;
            S[i][j] = (mask & s) ? up : down;
        }

        int sum = 0;

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                sum += S[i][j] * (S[i][(j + 1) % n] + S[(i + 1) % n][j]);
            }
        }

        H[s] = - J * ((double) sum);

    }

    for (int s = 0; s < half_states_size; s++) {
        Z += 2*exp(- beta * H[s]);
    }

    double p = 0;
    double E = 0;
    double E2 = 0;

    for (int s = 0; s < half_states_size; s++) {
        p = exp(- beta * H[s]) / Z;

        E += 2 * H[s] * p;
        E2 += 2 * pow(H[s], 2) * p;
    }

    return pow(beta, 2) * (E2 - pow(E, 2)) / N;
}

int main(void)
{
    FILE *fp = fopen("out.txt", "w");

    int m = 100;
    double T0 = 0.1;
    double Tm = 5;

    int half_states_size = pow(2, N-1);
    double * H = malloc(half_states_size * sizeof(double));

    for (int i = 0; i < m; i++) {
        double T = T0 + i * (Tm - T0)/m;
        double c = specific_heat(1/T, H, half_states_size);

        fprintf(fp, "%lf %lf\n", T, c);
    }

    free(H);

    fclose(fp);

    return 0;
}
