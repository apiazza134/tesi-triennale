![build status](https://gitlab.com/apiazza134/tesi-triennale/badges/master/pipeline.svg)

# Tesi triennale
Sono disponibili i PDF più recenti dell [long abstract](https://gitlab.com/apiazza134/tesi-triennale/-/jobs/artifacts/master/raw/long-abstract/long-abstract.pdf?job=compile_abstract), della [presentazione](https://gitlab.com/apiazza134/tesi-triennale/-/jobs/artifacts/master/raw/beamer/presentazione.pdf?job=compile_slides) e delle [riflessioni](https://gitlab.com/apiazza134/tesi-triennale/-/jobs/artifacts/master/raw/riflessioni/note.pdf?job=compile_notes).

## Simulazioni
### Isign 2D
[Video](https://gitlab.com/apiazza134/tesi-triennale/-/jobs/artifacts/master/raw/simulazioni/ising-2d/build/ising-2d.mp4?job=ising-2d:video) di una simulazione di Ising 2D.

## Libreria C `pcg`
Come generatore di numeri casuali non uso la funzione `rand()` della `stdlib` ma la libreria [pcg-random.org](https://www.pcg-random.org). La libreria è messa come sottomodulo quindi per scaricarla basta fare `git submodule update --init`. Per installarla andare nella cartella `simulazioni/lib/pcg` e dare
```bash
$ make all
$ install src/libpcg_random.a /usr/local/lib
$ install -m 0644 include/pcg_variants.h /usr/local/include
```

## Pacchetto `utils`
Ho raccolto in un pacchetto python minimale un po' di utilità per fare grafici e analisi dei dati. Per installarlo prima di tutto consiglio di creare un virtual environment: nell cartella `simulazioni` dare
```bash
$ virtualenv -p python3 venv
$ source venv/bin/activate
```
Poi andare nella cartella `simulazioni/utils` e installare il pacchetto con
```bash
$ python setup.py install
```
