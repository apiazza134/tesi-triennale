\documentclass[mathserif]{beamer}

\usepackage[T1]{fontenc}
\usepackage[italian]{babel}
\usepackage[utf8]{inputenc}

\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsfonts}
\usepackage{mathrsfs}
\usepackage{mathtools}
\usepackage{commath}
\usepackage{braket}
\usepackage[euler-digits]{eulervm}

\usepackage[style=alphabetic]{biblatex}
\addbibresource{reference.bib}

\usepackage{booktabs}

% Beamer config
\usetheme{Madrid}
\usecolortheme{dolphin}

\DeclareMathOperator{\tr}{tr}
\DeclareMathOperator{\Var}{Var}

\setbeamertemplate{bibliography item}{
    \ifboolexpr{
        test {\ifentrytype{book}}
        or test {\ifentrytype{mvbook}}
        or test {\ifentrytype{collection}}
        or test {\ifentrytype{mvcollection}}
        or test {\ifentrytype{reference}}
        or test {\ifentrytype{mvreference}}
    }
    {
        \setbeamertemplate{bibliography item}[book]
    }
    {
        \ifentrytype{online}
        {\setbeamertemplate{bibliography item}[online]}
        {\setbeamertemplate{bibliography item}[article]}
    }
    \usebeamertemplate{bibliography item}
}

\title[Simulazioni MC per il modello di Ising]{
    Simulazioni Monte Carlo per il modello di Ising
}
\author[Alessandro Piazza]{
    Alessandro Piazza \\
    \textcolor{gray}{Relatore:} Claudio Bonati
}
\date[17 settembre 2020]{
    Anno Accademico 2019/2020
}
\institute[]{
    Dipartimento di Fisica ``Enrico Fermi'' \\
    Università di Pisa
}
\titlegraphic{
    \vfill
    \includegraphics[scale=0.15]{img/unipi-logo}
}

\begin{document}
\begin{frame}
    \titlepage{}
\end{frame}
% \begin{frame}
%     \tableofcontents
% \end{frame}

\section{Modello di Ising}
\begin{frame}[b]{\secname}
    \begin{equation*}
        H = - J \sum_{\langle i, j \rangle} s_i s_j \,\textcolor{gray}{+ \, B \sum_{i} s_{i}} \qquad s_{i} = \pm 1
    \end{equation*}
    \begin{equation*}
        \text{Parametro d'ordine:} \quad
        m = \frac{1}{N} \sum_{i} s_{i} \qquad
        \chi = \dpd{\!\braket{m}}{B} = \frac{N}{kT}\left(\braket{m^{2}} - \braket{m}^{2}\right)
    \end{equation*}


    \pause{}
    Perché Ising 2D sul reticolo quadrato?
    \begin{itemize}
        \item Prototipo di sistema con una transizione di fase del secondo ordine: per \( t = (T - T_{c})/T_{c} \to 0 \)
        \begin{equation*}
            \xi \sim |t|^{-\nu} \qquad
            \braket{m} \sim \vartheta(-t) \, |t|^{-\beta} \qquad
            \chi \sim |t|^{\gamma}
        \end{equation*}
        ove \( \braket{s_{i} s_{j}} - \braket{m}^{2} \sim e^{-d(i, j)/\xi} \) per \( d(i, j) \to +\infty \)

        \pause{}
        \item Gli \emph{esponenti critici} dipendono solo dai parametri ``cinematici'' come simmetrie e dimensionalità del sistema, fatto noto come \emph{universalità}.

        \pause{}
        \item Sistema ben studiato, esiste una soluzione analitica, costo computazionale non elevato \( \Rightarrow \) palestra per una simulazione Monte Carlo
    \end{itemize}

    \hfill
\end{frame}

\section{Importance sampling}
\begin{frame}{\secname}
    % \begin{block}{}
    %     \begin{quote}
    %         All you have to do is compute the partition function \\
    %         \hfill -- A. Sagnotti
    %     \end{quote}
    % \end{block}
    % \vfill
    Sia $ \Lambda $ lo spazio degli stati. Nell'\emph{ensemble} canonico la media termodinamica di un osservabile $ \mathcal{O} \colon \Lambda \to \mathbb{R} $ è
    \begin{equation*}
        \braket{\mathcal{O}} = \tr{\left[\mathcal{O} \, \frac{e^{-\beta H}}{\mathcal{Z}}\right]}
        = \sum_{\mu \in \Lambda} \mathcal{O}(\mu) \frac{e^{-\beta E_{\mu}}}{\mathcal{Z}}
        \qquad
        \mathcal{Z} = \tr{\left[e^{-\beta H}\right]}
    \end{equation*}

    \pause{}
    In Ising 2D su reticolo quadrato il numero di stati scala come \( |\Lambda| = 2^{L^{2}} \)! \\
    Numericamente estraggo \( \mu_{1}, \ldots, \mu_{N} \) stati distribuiti come \( e^{-\beta E_{\mu}}/\,\mathcal{Z} \) e calcolo
    \begin{equation*}
        \mathcal{O}_{N} = \frac{1}{N} \sum_{k=1}^{N} \mathcal{O}(\mu_{k})
    \end{equation*}

    \pause{}
    \begin{block}{Catena di Markov}
        Ad ogni step ho una probabilità  \( P(\mu \to \nu) \) di cambiare stato in \( \Lambda \). Se la catena è ergodica, nel limite la distribuzione di probabilità sugli stati converge a una distribuzione \( {(p_{\mu})}_{\mu \in \Lambda} \) tale che \( p_{\mu} = \sum_{\nu \to \mu} p_{\nu} P(\nu \to \mu) \).
    \end{block}
    \hfill
\end{frame}

\section{Algoritmo di Metropolis}
\begin{frame}{\secname}
    Costruisco una catena di Markov sullo spazio degli stati del sistema fisico (configurazioni di spin per Ising) che converga a Boltzmann.

    \begin{block}{Bilancio dettagliato}
        \vspace{-12pt}
        \begin{equation*}
            p_{\mu} P(\mu \to \nu) = p_{\nu}P(\nu \to \mu) \quad \Rightarrow \quad
            \frac{P(\mu \to \nu)}{P(\nu \to \mu)} = \frac{p_{\nu}}{p_{\mu}} = e^{-\beta(E_{\nu} - E_{\mu})}
        \end{equation*}
    \end{block}

    \pause{}

    \vfill

    \begin{columns}
        \begin{column}{.65\textwidth}
            Pongo \( P(\mu \to \nu) = g(\mu \to \nu) A(\mu \to \nu) \), prendo \( g(\mu \to \nu) \) uniforme sugli stati che differiscono di uno \emph{spin-flip} e scelgo l'\emph{acceptance ratio}
            \begin{equation*}
                A(\mu \to \nu) =
                \begin{cases}
                    1 & E_{\nu} \leq E_{\mu} \\
                    e^{-\beta(E_{\nu} - E_{\mu})} & E_{\nu} > E_{\mu}
                \end{cases}
            \end{equation*}
        \end{column}
        \begin{column}[c]{.35\textwidth}
            \centering
            \includegraphics[width=0.8\columnwidth]{img/ising-2d-880}
        \end{column}
    \end{columns}
    \vfill
    Implementazione in \texttt{C} dell'algoritmo di Metropolis, disponibile su \url{https://gitlab.com/apiazza134/tesi-triennale}.
\end{frame}

\def\resultssecname{Magnetizzazione e suscettività}
\section{\resultssecname}

\def\magnetization{magnetization}
\def\susceptibility{susceptibility}

\begin{frame}{\secname}
    \hypertarget{results}{}

    Ridefinisco il parametro d'ordine per tenere conto di \( L \) finito e per togliere termini regolari alla transizione:
    \begin{equation*}
        \braket{m} \to \braket{|m|} \qquad
        \chi = \frac{N}{kT} \Var[m] \to \chi = N \Var\!\left[|m|\right]
    \end{equation*}

    Le misure estratte dalla catena di Markov sono intrinsecamente correlate e l'estimatore della varianza è non banale \( \Rightarrow \) \emph{blocking} \( + \) \emph{bootstrap}.

    \begin{columns}
        \def\width{\columnwidth}
        \begin{column}{.5\textwidth}
            \hyperlink{\magnetization}{
                \includegraphics[width=\width]{img/\magnetization}
            }
        \end{column}
        \begin{column}{.5\textwidth}
            \hyperlink{\susceptibility}{
                \includegraphics[width=\width]{img/\susceptibility}
            }
        \end{column}
    \end{columns}
\end{frame}

\def\fsssecname{Finite size scaling}
\section{\fsssecname}
\def\susceptibilityL{susceptibility-L}
\def\fss{fss}
\def\fssgamma{fss-gamma}
\begin{frame}[b]{\secname}
    \hypertarget{fss}{}

    Nel limite termodinamico \( t = (T - T_{c})/T_{c}\to 0 \Rightarrow \xi \sim |t|^{-\nu} \to +\infty \), ma se \( L \) è finito, \( \xi \to L \). La suscettività si scrive quindi come
    \begin{equation*}
        \chi \sim |t|^{\gamma} \sim \xi^{\gamma/\nu} \to L^{\gamma/\nu} \quad \Rightarrow \quad \chi(t, L) = L^{\gamma/\nu} \tilde{\chi}(\xi/L) = L^{\gamma/\nu} \chi_{0}(L^{1/\nu} |t|)
    \end{equation*}
    La soluzione analitica dà \( T_{c} = 2 J / \log(1 + \sqrt{2}) \), \( \nu = 1 \), \( \gamma = 7/4 \). \\

    \vfill

    \def\width{\columnwidth}
    \begin{columns}
        \begin{column}[c]{.5\textwidth}
            \only<+>{
                \hyperlink{\susceptibilityL}{
                    \includegraphics[width=\columnwidth]{img/\susceptibilityL}
                }
            }
            \only<+>{
                \hyperlink{\fss}{
                    \includegraphics[width=\columnwidth]{img/\fss}
                }
            }
        \end{column}

        \begin{column}[c]{.5\textwidth}
            \hyperlink{\fssgamma}{
                \includegraphics[width=\columnwidth]{img/\fssgamma}
            }
        \end{column}
    \end{columns}

    \hfill
\end{frame}

\begin{frame}[c]
    \begin{center}
        \huge{
            Grazie per l'attenzione!
        }
    \end{center}

    \vfill

    \nocite{*}
    \printbibliography{}
\end{frame}

\setbeamertemplate{page number in head/foot}{\phantom{ 0/0}}
\def\height{0.88\textheight}

\appendix
\section{Equilibrio del sistema}
\begin{frame}[noframenumbering]{\secname}
    \begin{columns}
        \begin{column}{.5\textwidth}
            \centering
            \includegraphics[width=\columnwidth]{img/eq-energy}
        \end{column}

        \begin{column}{.5\textwidth}
            \centering
            \includegraphics[width=\columnwidth]{img/eq-magnetization}
        \end{column}
    \end{columns}
\end{frame}

\section{Correlazione temporale}
\begin{frame}[noframenumbering,c]{\secname}
    \centering
    \includegraphics[height=\height]{img/correlation}
\end{frame}

\section{Energia e calore specifico}
\begin{frame}[noframenumbering]{\secname}
    \begin{columns}
        \begin{column}{.5\textwidth}
            \includegraphics[width=\columnwidth]{img/energy}
        \end{column}

        \begin{column}{.5\textwidth}
            \includegraphics[width=\columnwidth]{img/specific-heat}
        \end{column}
    \end{columns}
\end{frame}

\section{Blocking e Bootstrap}
\begin{frame}[noframenumbering]{\secname}
    \begin{columns}
        \begin{column}{.5\textwidth}
            \includegraphics[width=\columnwidth]{img/blocking-block-size}
        \end{column}

        \begin{column}{.5\textwidth}
            \includegraphics[width=\columnwidth]{img/bootstrap-resampling-size}
        \end{column}
    \end{columns}
\end{frame}


\begin{frame}[noframenumbering,c]{\resultssecname}
    \hypertarget{\magnetization}{}
    \centering
    \hyperlink{results}{
        \includegraphics[height=\height]{img/\magnetization}
    }
\end{frame}
\begin{frame}[noframenumbering,c]{\resultssecname}
    \hypertarget{\susceptibility}{}
    \centering
    \hyperlink{results}{
        \includegraphics[height=\height]{img/\susceptibility}
    }
\end{frame}

\begin{frame}[noframenumbering,c]{\fsssecname}
    \hypertarget{\susceptibilityL}{}
    \centering
    \hyperlink{fss}{
        \includegraphics[height=\height]{img/\susceptibilityL}
    }
\end{frame}
\begin{frame}[noframenumbering,c]{\fsssecname}
    \hypertarget{\fss}{}
    \centering
    \hyperlink{fss}{
        \includegraphics[height=\height]{img/\fss}
    }
\end{frame}
\begin{frame}[noframenumbering,c]{\fsssecname}
    \hypertarget{\fssgamma}{}
    \centering

    \hyperlink{fss}{
        \includegraphics[height=\height]{img/\fssgamma}
    }
\end{frame}

\end{document}